# dockerization for momo's projects

> see [docker-compose.yml](./docker-compose.yml)

## node-app docker image

in ./node-app you find a Docker image, which can host a node application.

## ./db

here you'll find the actual files for the mysql database. This may be used for backups. Beyond that, it should be ignored.

### running

```bash
# spawn the images with docker-compose
docker-compose up -d
```

> coastwards will be available at http://localhost:9090 and evoked will run at http://localhost:9091

In order to forward traffic from host to the node process, see the nginx config in [./nginx/coastwards.org.conf](./nginx/coastwards.org.conf) and  [./nginx/evoked.org.conf](./nginx/evoked.org.conf)
