#!/usr/bin/env bash

SCRIPT_DIR="$(cd "$(dirname "$0")"; pwd)"

main() {
    docker build --tag teedocker/node-app:latest --build-arg REPOSITORY="https://github.com/maureentsakiris/coastwards.git" .
}

main ${@}